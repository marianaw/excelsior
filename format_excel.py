import openpyxl
from openpyxl.cell import get_column_letter
from openpyxl.worksheet import Worksheet
from openpyxl.styles import Fill, Border
from openpyxl.style import Color
from copy import copy

#CAREFUL! Indexes in pandas and openpyxl are not the same.

def get_cell_dimensions (ws):
    '''
    Gets the dimensions of the cells from a worksheet.
    Parameters:
    @param ws: the worksheet whose dimensions are to be copyed.
    '''
    col_width = ws.column_dimensions['B'].width + 10
    row_height = ws.row_dimensions[1].height
    return col_width, row_height

def get_row_format(ws, numrow):
    '''
    Gets the format of the numrow-th row of excel_name.
    Parameters:
    @param ws: worksheet whose format is to be extracted.
    @param numrow: the number of the row whose style is to be fetched.
    Returns:
    A list containing the format of each cell, the column width and the row height.
    '''
    style_list = [ws.cell(row=numrow, column=i).style for i in range(0, ws.get_highest_column()+1)]
    return style_list
    
def get_column_format(ws, numcol):
    '''
    Gets the format of the numcol-th column of excel_name.
    Parameters:
    @param ws: worksheet whose format is to be extracted.
    @param numcol: the number of the column whose style is to be fetched.
    Returns:
    A list containing the format of each cell, the column width and the row height.
    '''
    style_list = [ws.cell(row=i, column=numcol).style for i in range(0, ws.get_highest_row())]
    return style_list
  
def copy_style_to_cell(ws, style, cell):
    '''
    Fetchs style from cell1 and copyes it to cell2.
    Parameters:
    @param ws: the worksheet we are working with.
    @param style: the style for the cell.
    @param cell: the cell to which copy style.
    '''
    ws._styles[cell.get_coordinate()] = copy(style)


def format_excel(index_format, name_format, standard_format, col_width, row_height, excel_name):
    '''
    Provide the appropriate format to the file specified by excel_name.
    Parameters:
    @param excel_name: name of the excel spreadsheet to be formatted.
    @param row_height: the desired height of the row.
    @param col_width: the desired column width.
    @param standard_format: format for data row.
    @param name_format: format for the comments fields.
    @param index_format: format for the fields.
    '''
    wb = openpyxl.load_workbook(excel_name)
    ws = wb.worksheets[0]
    ws.cell(row=0, column=0).value= 'field'
    ws.cell(row=0, column=0).style.font.bold = True
    index = [ws.cell(row=i, column=0) for i in range(1,ws.get_highest_row()+1)]
    index_names = [ws.cell(row=i, column=1) for i in range(1,ws.get_highest_row()+1)]
    data_row = [ws.cell(row=i, column=2) for i in range(1,ws.get_highest_row()+1)]
    
    for i in range(1,ws.get_highest_row()):
        ws.row_dimensions[i].height=row_height
    
    for i in range(1, ws.get_highest_column()+1):
        ws.column_dimensions[get_column_letter(i)].width = col_width
    
    ran = min(len(index), len(index_format))
    for i in range(0,ran):
        style = index_format[i]
        cell = index[i]
        style_name = name_format[i]
        cell_name = index_names[i]
        standard_style = standard_format[i]
        data_cell = data_row[i]
        copy_style_to_cell(ws, style, cell)
        copy_style_to_cell(ws, style_name, cell_name)
        copy_style_to_cell(ws, standard_style, data_cell)
        cell.style.alignment.wrap_text = True
        cell_name.style.alignment.wrap_text = True
        data_cell.style.alignment.wrap_text = True
    wb.save(excel_name)

def styling_files(filename, files):
    '''
    Styles all files in files array.
    Parameters:
    @param filename: the name of the main file which format we are copying.
    @param files: a list of lists of file names to be styled, one list per spreadsheet.
    '''
    wbook = openpyxl.load_workbook(filename)
    print('Styling files. This may take a while...')
    for j in range(0, len(files)):
        wsheet = wbook.worksheets[j]
        col_width, row_height = get_cell_dimensions(wsheet)
        index_format = get_row_format(wsheet, 0)
        name_format = get_row_format(wsheet, 1)
        i = 2
        for excel_file in files[j]:
            standard_format = get_row_format(wsheet, i)
            format_excel(index_format, name_format, standard_format, col_width, row_height, excel_file)
            i = i+1
    print('Successfully styled files.')
        
#if __name__ == '__main__':
    #styling_files('/home/mariana/Documents/oDesk/ExcelScript_DirectoryCleanup/medecision_test2/Metadata Extraction_Medecision.xlsx',['/home/mariana/Documents/oDesk/ExcelScript_DirectoryCleanup/medecision_test2/IT/AETEA/aetea_vendor_services_agreement_20120127_metadata.xlsx'])