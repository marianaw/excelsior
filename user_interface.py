from tkinter import Tk, Frame, BOTH, Button
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import showerror, showinfo
from load_excels import load_files
from format_excel import styling_files
import os
import logging

class ExcelsiorUI (Frame):
  
  def __init__ (self, parent):
    Frame.__init__(self, parent, background = 'white')
    self.parent = parent
    self.initUI()
    self.qbutton = Button(self, text="Quit", command=parent.destroy)
    self.qbutton.place(x=205, y=200)

  def centerWindow(self):
    w = 500
    h = 300
    sw = self.parent.winfo_screenwidth()
    sh = self.parent.winfo_screenheight()
    x = (sw - w)/2
    y = (sh - h)/2
    self.parent.geometry('%dx%d+%d+%d' % (w, h, x, y))
  
  def browse(self):
    Tk().withdraw() 
    self.filename = askopenfilename(parent=self,title='Choose a file')
    
  def process_file(self):
    logging.basicConfig(filename='errorlog.log', level=logging.DEBUG, 
                        format='%(asctime)s %(levelname)s %(name)s %(message)s')
    logger = logging.getLogger('errorlogger')
    try:
      if self.filename:
        with open(self.filename, 'rb') as filename:
          showinfo ('Info', 'Loading...')
          files=load_files(filename.name)
          showinfo ('Info','Styling...')
          #import ipdb; ipdb.set_trace()
          styling_files(filename,files)
          showinfo ('Info', 'Ready!')
    except Exception as e:
      showerror('Error.', e)
      logger.error(e, exc_info=True)
  
    
  def initUI(self):
    self.parent.title("Excelsior")
    self.pack(fill=BOTH, expand=1)
    self.centerWindow()
    self.bbutton= Button(self, text="Browse", command=self.browse)
    self.bbutton.place(x=50, y=25)  
    self.cbutton= Button(self, text="Load excel files", command=self.process_file)
    self.cbutton.place(x=50, y=75)


def main():
  root = Tk()
  app = ExcelsiorUI(root)
  root.mainloop()  


if __name__ == '__main__':
    main()  
 