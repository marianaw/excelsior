import pandas as pd
import os
from os.path import join, dirname

def clean_renamed_file_name(file_name):
    '''
    Replace some characters by _.
    Parameters:
    @file_name: the name of the file to be cleaned.
    Returns: the name cleaned.
    '''
    file_name=file_name.replace('/', '_')
    file_name=file_name.replace(' ', '_')
    name = str.split(file_name, '\n')
    return name[0]

def load_files(file_name):
    '''
    The master function fro this module. It takes the name of the source file, 
    iterates over its spreadsheets and calls internal function load_file to
    create metadata files.
    Parameters:
    @param file_name: the name of the source file (it must be an excel file).
    Returns: a list of metadata file names.
    '''
    exdf = pd.ExcelFile(file_name)
    dfs = [exdf.parse(sheet) for sheet in exdf.sheet_names]
    l = []
    for df in dfs:
        l = l + [load_file(df, file_name)]
        
    print('------------------')
    print('Successfully loaded all files.')
    print('------------------')
    return l
  
def load_file(df, file_name):
    '''
    It creates metadata files out of the spreadsheet of the source file.
    Parameters:
    @param df: a data frame containing data from a particular spreadsheet
    of the source file.
    @param file_name: the name of the source file.
    Returns: a list of the metadata file names made out of this specific
    spreadsheet (tab of source file).
    '''
    row_names = df.ix[0,]
    df = df[1:]
    new_file_names = []
    found = False

    for i,row in df.iterrows():
        renamed_file_name = str(row[len(df.columns)-1])
        for root, dirs, files in os.walk(dirname(file_name)):
            if renamed_file_name in files:
                renamed_file_name = clean_renamed_file_name(renamed_file_name)
                path = join(root, renamed_file_name)
                found = True
                break
        if(found):    
            r_file_name = path[:-4]
            
        else:
            r_file_name = renamed_file_name[:-4]
            print ('Not found')
            
        r_file_name = r_file_name + '_metadata.xlsx'
        new_file_names += [r_file_name]
        new_file = pd.concat([row_names,row], axis=1)
        new_file.columns = ['comment', 'value']
        new_file.to_excel(r_file_name)
        print('Loaded: ', r_file_name)
    return new_file_names

